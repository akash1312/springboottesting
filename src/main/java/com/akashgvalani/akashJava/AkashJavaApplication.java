package com.akashgvalani.akashJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class AkashJavaApplication {

	@RequestMapping("/")
	String home() {
		return "Success! Testing Spring Boot pipeline! Hooray";
	}

	public static void main(String[] args) {
		SpringApplication.run(AkashJavaApplication.class, args);
	}

}
